var matched, browser;

	jQuery.uaMatch = function( ua ) {
		ua = ua.toLowerCase();
	
		var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
			/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
			/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
			/(msie) ([\w.]+)/.exec( ua ) ||
			ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
			[];
	
		return {
			browser: match[ 1 ] || "",
			version: match[ 2 ] || "0"
		};
	};
	
	matched = jQuery.uaMatch( navigator.userAgent );
	browser = {};
	
	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}
	
	// Chrome is Webkit, but Webkit is also Safari.
	if ( browser.chrome ) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}
	
	jQuery.browser = browser;
	
function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}
function OnButton (){
    document.getElementById('samenfrm').submit();             // Submit the page
    return true;
}
jQuery(document).ready(function(){
	jQuery('div#menu ul').superfish({
		delay:       300,                            // one second delay on mouseout 
		animation:   {'marginLeft':'0px',opacity:'show',height:'show'},  // fade-in and slide-down animation 
		speed:       'fast',                          // faster animation speed 
		autoArrows:  false,                           // disable generation of arrow mark-up 
		onBeforeShow:      function(){ this.css('marginLeft','20px'); },
		dropShadows: false                            // disable drop shadows 
	});
	
	jQuery('.wine-temp').each(function(){
		var data = jQuery(this).html();
		jQuery("#pagecontent #crumbs").after("<ul class='wine-navigation'>" + data + "</ul>");
		jQuery("#pagecontent h1").css({'clear':'both','margin-bottom':'10px'});
	});
	
	jQuery('.wine-temp').remove();
	
	jQuery('#leftnav ul li ul li').each(function(){
		var hre_menu = jQuery(this).find('a').attr('href');	
		var path = window.location.pathname;
		
		if (path == hre_menu) {
			jQuery(this).addClass('active');
		}
	});
	var path = window.location.pathname;
	var arr = ["geboortekaartjes-meisje", 
				"geboortekaartjes-jongen", 
				"vintage-geboortekaartjes", 
				"getekende-geboortekaartjes", 
				"geboortekaartjes-tweeling", 
				"hippe-geboortekaartjes", 
				"stoere-geboortekaartjes", 
				"lieve-geboortekaartjes", 
				"klassieke-geboortekaartjes",
				"geboortekaartjes-top10",
				"geboortekaartjes-meisje"
				];
		
	jQuery('#menu li').each(function(){
		var hre_menu = jQuery(this).find('a').attr('href');	
		
		var path_arr = path.split("/");
		var hre_menu_arr = hre_menu.split("/");
		
		if (path_arr[1] == hre_menu_arr[1]) {
			jQuery(this).addClass('active');
		}
		if (path_arr[1] == 'zakelijke-kerstkaarten') {
			if (hre_menu_arr[1] == 'zakelijke_kerstkaarten') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'kerstkaarten') {
			if (hre_menu_arr[1] == 'zakelijke_kerstkaarten') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'zakelijke_nieuwjaarswens') {
			if (hre_menu_arr[1] == 'zakelijke_kerstkaarten') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'wijnen_kaartje') {
			if (hre_menu_arr[1] == 'kaart_en_kado') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'chocolade_kaartje') {
			if (hre_menu_arr[1] == 'kaart_en_kado') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'babykado_kaartje') {
			if (hre_menu_arr[1] == 'kaart_en_kado') {
				jQuery(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'bloemen_kaartje') {
			if (hre_menu_arr[1] == 'kaart_en_kado') {
				jQuery(this).addClass('active');
			}
		} 
		if (jQuery.inArray( path_arr[1], arr ) > 0) {
			jQuery('body').addClass('more_categories');
			if (hre_menu_arr[1] == 'geboortekaartjes') {
				jQuery(this).addClass('active');
			}
		}
		if (path_arr[1] == 'geboortekaartjes-meisje') {
			if (hre_menu_arr[1] == 'geboortekaartjes') {
				jQuery(this).addClass('active');
			}
		} 
	});
	
	
	var tophead = jQuery(".fixed").html();
	var headermenu = '<div class="headermenu">' + jQuery('#headermenu').html() + '</div>';
	jQuery('body #wrapper').before("<div class='tophead'><div class='topheadinner'>" + tophead + headermenu + "</div></div>");
	jQuery('#headermenu').remove();	
	jQuery('#header .fixed').remove();
	
	jQuery('#body_home #categories').remove()
	jQuery('#pagecontent-before').remove();
	jQuery('#pagecontent-after').remove();
	
	jQuery('#menu ul li,.left-menu li').each(function(){
		var url = window.location.pathname;
		var href = jQuery(this).find('a').attr('href');
		if (url == href) {
			jQuery(this).addClass('active');
		}
	});
	
	jQuery('.card-list li').hover(function(){
		jQuery(this).addClass('hover');
	}, function(){
		jQuery(this).removeClass('hover');	
	});
	
	jQuery('.card-list li').click(function(){
		window.location.assign(jQuery(this).find('a').attr('href'));
	});
	
	
	jQuery("#mainmenu li h3 a").click(function(event){
		event.preventDefault();
	});
	var pathname = window.location.pathname;
	pathname_a = pathname.split("/");
	var l = pathname_a.length;
	jQuery("#mainmenu li ul li ul li").each(function(){		
		var at = jQuery(this).find('a').attr('href');
		var at_a = at.split("/");
		if (at == pathname) {
			jQuery(this).find('a').parent().parent().parent().parent().prev().addClass('active');
			jQuery(this).find('a').parent().addClass('active');
		} else {
			
			if (pathname_a[1] == at_a[1]) {
				if (pathname_a[2] == at_a[2]) {
					jQuery(this).find('a').parent().parent().prev().addClass('active');
						jQuery(this).find('a').parent().addClass('active');
				}
			} 
		}
	});
	
	if (jQuery("#categories").length > 0) {
		var envelop = jQuery("#leftnav ul .envelop").html();
		jQuery("#leftnav ul .envelop").remove();
		jQuery("#categories").append("<ul style='clear:both'><li>" + envelop + "</li></ul>");
	}
	
	jQuery("#mainmenu li").find("h3.active").parent().find('ul').show();
	jQuery("#mainmenu li h3").addClass("close");
	jQuery("#mainmenu li h3.active").addClass("open");
	jQuery("#mainmenu li h3").removeClass('active');
	
	jQuery("#mainmenu li h3").each(function(){
		var hre = jQuery(this).find('a').attr('href');
		if (hre == pathname) {
			jQuery(this).addClass("open");
			jQuery(this).parent().find('ul').show();
		}
	});
	
	jQuery("#mainmenu li .close").click(function(){
		var inner=jQuery(this).parents("li").find("ul"); 																														
		if(jQuery(this).hasClass("open")){				
			jQuery(this).removeClass("open");
			inner.slideUp(300);
			
		}else{
			jQuery("#mainmenu li ul").not(inner).hide();
			jQuery("#mainmenu li .close").removeClass("open");			
			jQuery(this).addClass("open");
			inner.slideDown(300);
		}															
	});
	
	if (pathname_a[1] != 'wenskaarten') {
		var url = location.href;
		var src1 = jQuery('#voor_preview img').attr('src');
		var src2 = jQuery('#binnen_preview img').attr('src');
	jQuery('#body_design #design').append('<form action="/samen" method="get" id="samenfrm"><input type="hidden" name="url" id="url" value="' + url + '"><input type="hidden" id="src1" name="src1" value="' + src1 + '"><input type="hidden" id="src2" name="src2" value="' + src2 + '"></form><div class="extra"><a href="javascript:;" onclick="return OnButton();" rel="nofollow">Ontwerp samen met ons</a></div>');
	}
	
	if (jQuery(".zoom").length){
		jQuery(".zoom").click(function(e){
			e.preventDefault();
			var html ='';
			var src2 = jQuery(this).parent().prev().attr('src');
			var src1 = jQuery(this).parent().prev().prev().attr('src');
			html += '<img src="' + src1 + '" alt="">';
			html += '<img src="' + src2 + '" alt="">';
			jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
			jQuery('body').append('<div id="lightbox"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span>' + html + '</div>').show('slow');
		});
	}
	jQuery('#categories .cate_more').click(function(){
		if (!jQuery(this).hasClass('selected')){
			jQuery(this).next('.left-menu').slideDown();
			jQuery(this).addClass('selected');
		} else {
			jQuery(this).next('.left-menu').slideUp();
			jQuery(this).removeClass('selected');
		}
	});
}); 