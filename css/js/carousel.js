//this is maximum number of items to display on each carousel
						   var MAX_ITEM_DISPLAY = 4;
							
							/** This function initializes the first carousel on the tabs */
						   function PageInit() {
							   //let's apply JCarouselLite to first tab content list.
							   SetCarouselOn('list_widget_1', MAX_ITEM_DISPLAY, 'sp_next_1', 'sp_prev_1');
						   }
							
							/** This function sets the carousel on any list of items on our page where it is called
							* @sDivId: this is the id of the control that contains the list of items on which we want to set the carousel.
							* @iVisibleItems:  this is the number of items that will be displayed at one time on the carousel.
							* @sNextButtonId: this is the id of the button on the right hand side that enables to slide the carousel to the right.
							* @sPrevButtonId: is the id of the button on the left hand side that slides the carousel to the left.
							*/
						   function SetCarouselOn(sDivId, iVisibleItems, sNextButtonId, sPrevButtonId) {
							   var divId = "#" + sDivId;//make up selector
							   var iNoProducts = $(divId + ' ul li').size();//count number of items in the list where we are applying the carousel.
							   //set carousel on list only the first time and only if we have more products than the maximum visible.           
							   if ($(divId).attr('rel') != 'set' && iNoProducts > iVisibleItems) {                  
								   $(divId).jCarouselLite({ visible: iVisibleItems, btnNext: "#" + sNextButtonId, btnPrev: "#" + sPrevButtonId });
								   $(divId).attr('rel', 'set');//remember that carousel has been set on this container
							   }
							   /*We don't want to show the left and right navigation buttons if we have a number of items
							   less or equal to the maximum number to show on the carousel*/
							   if (iNoProducts <= iVisibleItems) {
								   $('#' + sNextButtonId).hide();//hide right hand side navigation button
								   $('#' + sPrevButtonId).hide();//hide left hand side navigation button
							   }
						   }
				
							/* when our document is ready, let's do some initialization */
						   $(document).ready(function () {              
							   //Attach click event to all our tab header links
							   $("ul.tabs li a").click(function () {
								   $("ul.tabs li a").removeClass("on"); //Remove any "on" class (unselect) all links on tab headers
								   $(this).addClass("on"); //Add "on" class (select) to selected tab header
								   $(".tab_content").hide(); //Hide all tab contents
								   var activeTab = $(this).attr("href");//get selector for the tab content related to link we have just clicked
								   $(activeTab).fadeIn();//fade this tab content to show it
								   var iTabIndex = $(this).attr("rel");//get the index of this tab header
								   //use the index we just got to set carousel on related tab content
								   SetCarouselOn('list_widget_' + iTabIndex, MAX_ITEM_DISPLAY, 'sp_next_' + iTabIndex, 'sp_prev_' + iTabIndex);
								   return false;
							   });
								
							   PageInit();
							   
							   $('.bxslider').bxSlider({
                                mode: 'vertical',
                                //speed: 3000,
                                auto: true,
                                pager: true,
                                pagerCustom: '#customPager2',
                                controls: false
                            });
						   });